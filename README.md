## recipe-app-api-proxy
## DevOps Automation
NGINX proxy for recipe-app API

### Usage

#### Environment Variables

* `LISTEN_PORT` - Port to listen on NGINX proxy (default: `8080`)
* `APP_HOST` - Hostname of the app to forward request (default: `app`)
* `APP_PORT` - Forward requests to Port on `APP_HOST` (default: `9000`)
